#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

void* mythread_fun(void* value){
    int i;
    for (i=10; i<20;++i){
        printf("Second thread , value %d\n",i);
        sleep(2);
    }
}

int main(){
    pthread_t th;
    pthread_create(&th,NULL,mythread_fun,NULL);

    int i;
    for (i=0; i<10;i++){
        printf("First thread, value %d\n",i);
        sleep(1);
    }
    pthread_join(th,NULL);
    return 0;
}